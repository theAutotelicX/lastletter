package com.example.student.lastletter;

import android.content.pm.ActivityInfo;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.widget.TextView;

public class ScoreBoard extends AppCompatActivity {

    public String topScore = "select * from " + DatabaseScore.TABLE_NAME + " order by "+ DatabaseScore.COL_SCORE + " desc limit 10";
    private DatabaseScore mHelper;
    private SQLiteDatabase mDb;
    private Cursor mCursor;
    private TextView name;
    private TextView score;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_score_board);
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
        mHelper = new DatabaseScore(this);
        mDb = mHelper.getWritableDatabase();
        name = (TextView)findViewById(R.id.name);
        score = (TextView)findViewById(R.id.score);
        mCursor = mDb.rawQuery(topScore,null);
        mCursor.moveToFirst();
        name.setText("");
        score.setText("");
        String nameDB="";
        String scoreDB="";
        if (mCursor != null && mCursor.moveToFirst()){ //make sure you got results, and move to first row
            do{
                nameDB = mCursor.getString(1)+"\n"; //column 1 for the current row
                scoreDB = mCursor.getInt(2)+"\n"; //column 2 for the current row
                name.append(nameDB);
                score.append(scoreDB);

            } while (mCursor.moveToNext()); //move to next row in the query result
        }

    }

}
