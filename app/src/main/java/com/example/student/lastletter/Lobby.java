package com.example.student.lastletter;

import android.Manifest;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.os.IBinder;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListView;
import android.widget.Toast;

import java.lang.reflect.Method;

public class Lobby extends AppCompatActivity {

    Button btnScan,btnListen;
    static Button btnStart;
    ListView scanListView;

    Intent serviceIntent;
    Boolean mBound=false;
    BluetoothAdapter bluetoothAdapter;
    static ArrayAdapter<String> arrayAdapter;
    MonitorBluetooth monitor;
    BluetoothService mBluetoothService;

    private static final int REQUEST_COARSE_LOCATION = 1;

    protected void checkLocationPermission(){
        if (ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION)
                != PackageManager.PERMISSION_GRANTED) {

            ActivityCompat.requestPermissions(this,
                    new String[]{Manifest.permission.ACCESS_COARSE_LOCATION}, REQUEST_COARSE_LOCATION);
        }
        else if(ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION)
                == PackageManager.PERMISSION_GRANTED){
            if(mBound)
            {listenButton();}
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        switch (requestCode) {
            case REQUEST_COARSE_LOCATION: {
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    if(mBound)
                    {listenButton();}
                }
                else {
                    Intent gobackMainMenu = new Intent(this,MainActivity.class);
                    startActivity(gobackMainMenu);
                }
                break;
            }
        }
    }
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_lobby);
        findViewByIds();//register all ids
        bluetoothAdapter = BluetoothAdapter.getDefaultAdapter();
    }
    @Override
    protected void onStart() {
        //start service bluetoothService
        super.onStart();
        serviceIntent = new Intent(this,BluetoothService.class);
        startService(serviceIntent);
        bindService(serviceIntent,mConnection, Context.BIND_AUTO_CREATE);
        checkLocationPermission();
        btnStart.setEnabled(false);
    }

    @Override
    protected void onStop() {
        super.onStop();
        unbindService(mConnection);
        mBound = false;
    }

    @Override
    protected void onResume() {
        super.onResume();
        if(mBound)
            listenButton();
    }

    private void listenButton() {
        btnScan.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Toast.makeText(getApplicationContext(),"Searching",Toast.LENGTH_SHORT).show();
                BluetoothService.stringArrayList.clear();
                mBluetoothService.scanStart();
                arrayAdapter = new ArrayAdapter<String>(getApplicationContext(),android.R.layout.simple_list_item_1,BluetoothService.stringArrayList);
                scanListView.setAdapter(arrayAdapter);

            }
        });
        btnListen.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(bluetoothAdapter.isDiscovering()){
                    bluetoothAdapter.cancelDiscovery();
                }
                mBluetoothService.serverStart();
            }
        });
        scanListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                bluetoothAdapter.cancelDiscovery();
//                BluetoothDevice device = btArray[position];
//                Boolean isBonded = false;
//                try {
//                    isBonded = createBond(device);//TODO: create bond between device
//                    if(isBonded){
//                        ClientClass clientClass = new ClientClass(btArray[position]);
//                        clientClass.start();
//                        Toast.makeText(getApplicationContext(),"Connecting",Toast.LENGTH_SHORT).show();
//                    }
//                } catch (Exception e) {
//                    e.printStackTrace();
//                }
                mBluetoothService.clientStart(position);
            }
        });
        btnStart.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent multiplayer = new Intent(getApplicationContext(),Multiplayer.class);
                startActivity(multiplayer);
            }
        });
//        btnSend.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                String string = writeMessage.getText().toString();
//                sendReceive.write(string.getBytes());
//            }
//        });
    }
    public boolean createBond(BluetoothDevice btDevice) throws Exception{
        Class class1 = Class.forName("android.bluetooth.BluetoothDevice");
        Method createBondMethod = class1.getMethod("createBond");
        Boolean returnValue = (Boolean) createBondMethod.invoke(btDevice);
        return returnValue.booleanValue();
    }

    private void findViewByIds(){
        btnScan = (Button)findViewById(R.id.btnScan);
        btnListen = (Button)findViewById(R.id.btnHost);
        btnStart = (Button)findViewById(R.id.btnStart);
        scanListView = (ListView)findViewById(R.id.scanListView);
    }

    private ServiceConnection mConnection = new ServiceConnection() {
        @Override
        public void onServiceConnected(ComponentName name, IBinder service) {
            Log.d("BluetoothService","onServiceConnected!");
            mBound = true;
            BluetoothService.LocalBinder mLocalBinder = (BluetoothService.LocalBinder) service;
            mBluetoothService = (BluetoothService) mLocalBinder.getService();
        }

        @Override
        public void onServiceDisconnected(ComponentName name) {
            mBound = false;
            mBluetoothService = null;
        }
    };
//    private class SendReceive extends Thread{
//        private final BluetoothSocket bluetoothSocket;
//        private final InputStream inputStream;
//        private final OutputStream outputStream;
//
//        public SendReceive(BluetoothSocket socket)
//        {
//            bluetoothSocket = socket;
//            InputStream tempIn = null;
//            OutputStream tempOut = null;
//
//            try {
//                tempIn = bluetoothSocket.getInputStream();
//                tempOut = bluetoothSocket.getOutputStream();
//
//            } catch (IOException e) {
//                e.printStackTrace();
//            }
//            inputStream = tempIn;
//            outputStream = tempOut;
//        }
//        public void run(){
//            byte[] buffer = new byte[1024];
//            int bytes;
//
//            while(true){
//                try {
//                    bytes = inputStream.read(buffer);
//                    handler.obtainMessage(STATE_MESSAGE_RECEIVED,bytes,-1,buffer).sendToTarget();;
//                } catch (IOException e) {
//                    e.printStackTrace();
//                }
//            }
//        }
//        public void write(byte[] bytes){
//            try {
//                outputStream.write(bytes);
//            } catch (IOException e) {
//                e.printStackTrace();
//            }
//        }
//    }
}
