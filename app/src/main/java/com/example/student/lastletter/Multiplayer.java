package com.example.student.lastletter;

import android.app.AlertDialog;
import android.app.Dialog;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.content.BroadcastReceiver;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.ServiceConnection;
import android.content.pm.ActivityInfo;
import android.database.sqlite.SQLiteDatabase;
import android.graphics.Color;
import android.graphics.Typeface;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.os.Handler;
import android.os.IBinder;
import android.support.annotation.Nullable;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.Set;

public class Multiplayer extends AppCompatActivity {
    public Set dict = new HashSet<String>();
    private EditText inputText;
    private TextView nextLetter;
    private TextView singleScore;
    public static Button sendWordButton;
    private String prevWord = "";
    public static ArrayList usedWord = new ArrayList();
    private TextView time;
    public CountDownTimer countTime;
    private long secondleft;
    private Dialog pauseGame;
    private Dialog gameOver;
    private static String turn="";
    public static  ListView historyword;
    public static ArrayAdapter<String> collectedWord;
    private String nameInput;
    private DatabaseScore mHelper;
    private SQLiteDatabase mDb;
    private boolean checkTurnBaseMulti;
    public static final String RECEIVER_INTENT = "RECEIVER_INTENT";
    public static final String RECEIVER_MESSAGE = "RECEIVER_MESSAGE";
    Intent serviceIntent;
    Boolean mBound = false;
    BluetoothService mBluetoothService;
    private BluetoothDevice clientDevice = null;
    private BluetoothAdapter hostDevice = null;
    public static boolean myTurn = false;
    private boolean isClient = false;
    public static boolean iWin = false;
    private BroadcastReceiver mBroadcastReceiver;
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_multiplayer);
        findAllId();
        mBroadcastReceiver = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
                checkTurnBaseMulti = intent.getBooleanExtra(RECEIVER_MESSAGE,false);
            }
        };
    }

    @Override
    protected void onStart() {
        super.onStart();
        serviceIntent = new Intent(this,BluetoothService.class);
        //startService(serviceIntent);
        bindService(serviceIntent,mConnection, Context.BIND_AUTO_CREATE);
        Toast.makeText(getApplicationContext(),"Intializing",Toast.LENGTH_SHORT).show();
        LocalBroadcastManager.getInstance(this).registerReceiver((mBroadcastReceiver),new IntentFilter(RECEIVER_INTENT));
        collectedWord = new ArrayAdapter<String>(this,android.R.layout.simple_dropdown_item_1line,usedWord);
        historyword.setAdapter(collectedWord);
        final Handler handler = new Handler();
        handler.postDelayed(new Runnable() {
            @Override
            public void run() {
                //Do something after 100ms
                checkTurnBase();
                if(iWin){
                getWinner();
                iWin = false;
                }
                handler.postDelayed(this, 700);
            }
        }, 700);
    }
    private void findAllId(){
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
        readDict("engDict.bin");
        sendWordButton = (Button)findViewById(R.id.SendWord);
        nextLetter = (TextView)findViewById(R.id.nextLetter);
        singleScore = (TextView)findViewById(R.id.singleScore);
        pauseGame = new Dialog(this);
        gameOver = new Dialog(this);
        historyword = (ListView)findViewById(R.id.historyword);
        time =(TextView) findViewById(R.id.time);
        inputText = (EditText) findViewById(R.id.inputText);
        //score=0;//fixed score if they use previous one
        mHelper = new DatabaseScore(this);
        mDb = mHelper.getWritableDatabase();

    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        usedWord.clear();
        collectedWord.notifyDataSetChanged();
    }
    private void allSystemStart(){
        //from after receive mBound complete
        checkHostClientDevice();
        checkClient();
        turnBaseMulti();
    }
    private void listenButton(){
        sendWordButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                readInput();
            }
        });
    }
    private void checkHostClientDevice(){
        if(mBound){//need loop until check() work
            try{
                clientDevice = mBluetoothService.getBtClientDevice();
            }
            catch (Exception e){
                //nullpointer exception
                hostDevice = mBluetoothService.getBtServerDevice();
            }
        }
    }
    private void checkTurnBase(){
        if(checkTurnBaseMulti){
            turnBaseMulti();
            String nextNewLetter = getLastLetter((String) usedWord.get(usedWord.size()-1));
            nextLetter.setText("WELL DONE! Your next Letter is " + nextNewLetter);
            prevWord = (String) usedWord.get(usedWord.size()-1);
            checkTurnBaseMulti = false;
        }
    }
    private void turnBaseMulti(){
        //client start first
        if(myTurn){
            //myTurn : client turn
            secondleft = 10;
            time.setText("10");
            turn="My turn";
            singleScore.setText(turn);
            listenButton();
            timeGame();
            myTurn = false;
        }
        else{
            //hostTurn
            timeGame();
            countTime.cancel();
            time.setText("10");
            secondleft = 10;
            turn="Other turn";
            singleScore.setText(turn);
            sendWordButton.setEnabled(false);
        }
    }
    private void checkClient(){
        if(clientDevice!=null){
            //check if this device is client
            isClient = true;
            myTurn = true;
            turn="My turn";
            singleScore.setText(turn);
        }
        else{
            isClient = false;
            turn="Other turn";
            singleScore.setText(turn);
        }
    }
    public void getWinner(){
        AlertDialog gameoverDialog = new AlertDialog.Builder(this).create();
        gameoverDialog.setCanceledOnTouchOutside(false); //prevent user click other place from dialog
        gameoverDialog.setCancelable(false); //prevent user use go back button to continue game
        gameoverDialog.setTitle("Game Over");
        gameoverDialog.setMessage("You win");
        gameoverDialog.show();
    }
    @Override
    protected void onStop() {
        super.onStop();
        if(mBound){
            unbindService(mConnection);
            mBound = false;
        }
        LocalBroadcastManager.getInstance(this).unregisterReceiver(mBroadcastReceiver);
    }
    private ServiceConnection mConnection = new ServiceConnection() {
        @Override
        public void onServiceConnected(ComponentName name, IBinder service) {
            Log.d("BluetoothService","onServiceConnected!");
            mBound = true;
            BluetoothService.LocalBinder mLocalBinder = (BluetoothService.LocalBinder) service;
            mBluetoothService = (BluetoothService) mLocalBinder.getService();
            allSystemStart();
        }

        @Override
        public void onServiceDisconnected(ComponentName name) {
            mBound = false;
            mBluetoothService = null;
        }
    };
    private void readDict(String fileName){
        String text = "";
        try{
            InputStream is = getAssets().open(fileName);
            BufferedReader reader = new BufferedReader(new InputStreamReader(is));
            String line = reader.readLine();
            while(line!=null){
                dict.add(line);
                line = reader.readLine();
            }
            is.close();
        }
        catch(IOException e){
            e.printStackTrace();
        }
    }
    private String getLastLetter(String word){
        return word.substring(word.length()-1);
    }
    private boolean compareFirstLastLetter(String prevWord,String newWord){
        if(prevWord.equals("")){
            usedWord.add(0,inputText.getText().toString().toLowerCase());
            countTime.cancel();
            timeGame();
            return true;
        }
        String prevLastLetter = getLastLetter(prevWord);
        String newFirstLetter = newWord.substring(0,1);

        if(prevLastLetter.equals(newFirstLetter)){
            usedWord.add(0,inputText.getText().toString().toLowerCase());
            countTime.cancel();
            timeGame();
            return true;
        }
        return false;
    }
    private void timeGame(){// set timer of the game
        time.setTextColor(Color.WHITE);
        time.setTypeface(Typeface.DEFAULT);
        countTime = new CountDownTimer(11000,1000) {
            @Override
            public void onTick(long millisUntilFinished) {
                secondleft = millisUntilFinished / 1000;
                if(secondleft==1){// change color when almost timeup
                    time.setTextColor(Color.RED);
                    time.setTypeface(Typeface.DEFAULT_BOLD);
                    //time.setBackgroundColor(Color.GRAY);
                }
                time.setText(Long.toString(secondleft));
            }

            @Override
            public void onFinish() {
                time.setText("0");
                gameOver();
            }
        };
        countTime.start();
    }
    private void gameOver(){//need to get who win
        countTime.cancel();
        time.setText("0");
        mBluetoothService.sendReceive.write("I lose".getBytes());
        //TextView endscore = (TextView)findViewById(R.id.endscore);
        //endscore.setText(score);

        AlertDialog gameoverDialog = new AlertDialog.Builder(this).create();
        gameoverDialog.setCanceledOnTouchOutside(false); //prevent user click other place from dialog
        gameoverDialog.setCancelable(false); //prevent user use go back button to continue game
        gameoverDialog.setTitle("Game Over");
        //gameoverDialog.setMessage("\nYour score is "+score+"\n\nPlease enter your name");
        gameoverDialog.setMessage("You lose");
//        final EditText name = new EditText(this);
//        gameoverDialog.setView(name);
//        gameoverDialog.setButton(AlertDialog.BUTTON_NEUTRAL, "OK",
//                new DialogInterface.OnClickListener() {
//                    public void onClick(DialogInterface dialog, int which) { //after click ok button, it will link to scoreboard page
//                        nameInput = name.getText().toString();
//                        dialog.dismiss();
//                        //sendScore();
//                        endGame();
//                    }
//                });

        //gameOver.setContentView(R.layout.gameover);
        //gameOver.setCanceledOnTouchOutside(false);
        gameoverDialog.show();
    }
    public void endGame(){
        Intent scoreBoard = new Intent(this, ScoreBoard.class);
        startActivity(scoreBoard);
        this.finish(); //prevent user go back to game activity
    }
    public void showPause(View view){
        pauseGame.setContentView(R.layout.pause);
        String selectedButton = ((Button)view).getText().toString();

        if(selectedButton.equals("restart")) {
            countTime.cancel();
            Intent singlePlayer = new Intent(this, SinglePlayer.class);
            singlePlayer.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP); //prevent user goback
            startActivity(singlePlayer);
            finish();
        }
        else if(selectedButton.equals("exit")) {
            countTime.cancel();//end counttime
            Intent mainmenu = new Intent(this, MainActivity.class)
                    .setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP)
                    .setFlags(Intent.FLAG_ACTIVITY_NEW_TASK)
                    .setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
            startActivity(mainmenu);
            finish();
        }
        Button closepause = (Button) pauseGame.findViewById(R.id.closepause);
        closepause.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                pauseGame.dismiss();
            }
        });
        Button resume = (Button) pauseGame.findViewById(R.id.resume);
        resume.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                pauseGame.dismiss();
            }
        });
        pauseGame.show();
        if(secondleft==0){// prevent go back while gameover with pause dialog
            pauseGame.cancel();
        }
    }
    private void readInput(){
        String inputTextCorrection = inputText.getText().toString().toLowerCase();
        //if some stupid people just click without adding anything
        if(inputTextCorrection.equals("")){
            return;
        }
        String nextNewLetter = getLastLetter(inputTextCorrection);

        if(usedWord.contains(inputTextCorrection)) {
            nextLetter.setText("You have used this word. Please try again with "+ getLastLetter(prevWord));
            inputText.setText("");
        }
        else if (dict.contains(inputTextCorrection) && compareFirstLastLetter(prevWord, inputTextCorrection)) {// if inputText exist in dict and first letter match with previous
            //if pass
            prevWord = (String) usedWord.get(usedWord.size()-1);
            nextNewLetter = getLastLetter((String) usedWord.get(usedWord.size()-1));
            nextLetter.setText("WELL DONE! Your next Letter is " + nextNewLetter);
            //int sizeScore = inputTextCorrection.length();
            //int wordScore=sizeScore*100;
            addUsedWord();
            inputText.setText("");
            //broadcast to all
            mBluetoothService.sendReceive.write(inputTextCorrection.getBytes());// send message to other device
            //ask if send successful
            sendWordButton.setEnabled(false);//Lock send button
            countTime.cancel();
            time.setText("10");
            secondleft = 10;
            turnBaseMulti();
            //time reset
            //notify change in used word
        }
        else {
            //if wrong do
            if (prevWord.equals("")) //no intial word when starting the game
            {
                nextLetter.setText("T_T Please use word in dictionary");
            }
            else //
            {
                nextLetter.setText("NOPE! Please try again "+ getLastLetter(prevWord));
            }
            inputText.setText("");
        }
    }
    private void addUsedWord(){
        collectedWord.notifyDataSetChanged();
    }
    @Override
    public void onBackPressed() { //prevent back to mainmenu but timer still count caused crash
        countTime.cancel();
        Intent main = new Intent(this, MainActivity.class)
                .setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP)
                .setFlags(Intent.FLAG_ACTIVITY_NEW_TASK)
                .setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
        startActivity(main);
        finish();
    }

}
