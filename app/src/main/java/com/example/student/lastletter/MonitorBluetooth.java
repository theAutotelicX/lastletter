package com.example.student.lastletter;

import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.util.Log;

public class MonitorBluetooth extends Thread{
    BluetoothAdapter bluetooth;
    private BluetoothReceiver bReceiver;
    private Context mContext;

    public MonitorBluetooth(Context context){
        bluetooth=BluetoothAdapter.getDefaultAdapter();
        this.bReceiver=new BluetoothReceiver();
        this.mContext=context;
    }

    public void finalize() throws Throwable{
        super.finalize();
    }

    public void run(){
        IntentFilter filter1=new IntentFilter(BluetoothDevice.ACTION_FOUND);
        filter1.addAction(BluetoothDevice.ACTION_NAME_CHANGED);
        mContext.registerReceiver(this.bReceiver,filter1);
        IntentFilter filter2=new IntentFilter(BluetoothAdapter.ACTION_DISCOVERY_STARTED);
        mContext.registerReceiver(this.bReceiver,filter2);
        IntentFilter filter3=new IntentFilter(BluetoothAdapter.ACTION_DISCOVERY_FINISHED);
        mContext.registerReceiver(this.bReceiver,filter3);
        bluetooth=BluetoothAdapter.getDefaultAdapter();
        bluetooth.startDiscovery();
    }
    public void stopWorking(){
        bluetooth.cancelDiscovery();
        mContext.unregisterReceiver(bReceiver);
    }

    class BluetoothReceiver extends BroadcastReceiver {
        public void onReceive(Context context, Intent intent){
            String action=intent.getAction();
            if(BluetoothDevice.ACTION_FOUND.equals(action)){
                BluetoothDevice device=intent.getParcelableExtra(BluetoothDevice.EXTRA_DEVICE);
                if(device!=null){
                    if(BluetoothService.bt.contains(device))//check if the same device
                    {
                        ;
                    }
                    else{
                        BluetoothService.bt.add(device);
                    }
                }
                int index=0;
                BluetoothService.stringArrayList.clear();
                BluetoothService.btArray = new BluetoothDevice[BluetoothService.bt.size()];
                if(BluetoothService.bt.size()>0){
                    for (BluetoothDevice deviceBt:BluetoothService.bt){
                        Log.e("Bluetooth","Empty" + deviceBt.getName());
                        BluetoothService.btArray[index] = deviceBt;
                        String deviceName = deviceBt.getName();
                        if(deviceName == null){
                            ;
                        }
                        else{
                        BluetoothService.stringArrayList.add(deviceName);
                        index++;
                        }
                    }
                    Lobby.arrayAdapter.notifyDataSetChanged();
                    Log.d("Bluetooth",BluetoothService.bt.toString());
                }
                Log.d("BluetoothReceiver","found "+device.getAddress()+", "+device.getName());
            }
            else if(BluetoothAdapter.ACTION_DISCOVERY_STARTED.equals(action)){
                Log.d("BluetoothReceiver","discovery started");
            }
            else if(BluetoothAdapter.ACTION_DISCOVERY_FINISHED.equals(action)){
                Log.d("BluetoothReceiver","discovery finished");
                context.unregisterReceiver(this);
            }
        }
    }
}